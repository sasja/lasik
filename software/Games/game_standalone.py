#!/usr/bin/env python3

import re
import random
import threading
import time
import pygame
from pygame.locals import *
import queue
import os
from os.path import basename
import socket
import sys

class Color:
    BLACK =  (0,0,0)
    RED =    (255,0,0)
    GREEN =  (0,255,0)
    BLUE =   (0,0,255)
    YELLOW = (255,255,0)
    PURPLE = (255,0,255)
    CYAN =   (0,255,255)

class Laser:
    def __init__(self, name):
        self.name = name
        self.pwm = 0
        self.blink_p = None
        self.nextBlink = None
        self.blinkOn = True
        self.shutdown = False
        threading.Thread(target = self._t_blinkThread).start()
    def set(self, pwm):
        self.pwm = pwm
        self.nextBlink = None
        lasikService.send("set {} {}".format(self.name, self.pwm))
    def _t_blinkThread(self):
        while not self.shutdown:
            if self.nextBlink and time.time() >= self.nextBlink:
                self.nextBlink += self.blink_p
                self.blinkOn = not self.blinkOn
                lasikService.send("set {} {}".format(self.name, self.pwm if self.blinkOn else "0"))
            time.sleep(0.010)
    def blink(self, period):
        self.blink_p = period
        self.nextBlink = time.time() + self.blink_p
    def close(self):
        self.shutdown = True

def send(cmd):
    cmd += '\n'
    serverSocket.send(cmd.encode('ascii'))

class LasikService:
    def __init__(self, host = "localhost", port = 1337, timeout = 1):
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect((host,port))
            self.socket.settimeout(timeout)
        except ConnectionError:
            raise ConnectionError("could not connect to server socket, is the server running?")
        self.fromLasik = queue.Queue(maxsize = 1000)
        self.toLasik = queue.Queue(maxsize = 1000)
        self.shutdown = False
        self.timeout = timeout
        threading.Thread(target = self._t_listen).start()
        threading.Thread(target = self._t_send).start()

    def _t_listen(self): #TODO framing thing see _t_socketConnListener (socketserver.py)
        while not self.shutdown:
            try:
                msg = self.socket.recv(1024).decode('ascii').strip() # TODO errorhandling
            except socket.timeout:
                continue
            self.fromLasik.put(msg, block = False) # raises Full exception
        print("Shut down listen thread")

    def _t_send(self):
        while not self.shutdown:
            try:
                msg = self.toLasik.get(timeout = self.timeout)
            except queue.Empty:
                continue
            msg += '\n'
            self.socket.sendall(msg.encode('ascii')) # exception if blocks longer than timeout
        print("Shut down send thread")

    def send(self, msg):
        self.toLasik.put(msg, block = False) # raises Full exception

    def get(self):
        return self.fromLasik.get()

    def flushMsgQueue(self):
        try:
            while True:
                self.fromLasik.get(block = False)
        except queue.Empty:
            pass

    def close(self):
        self.shutdown = True
        print("Requesting LasikService shutdown")

score = 0

def t_consumer(): # TODO this thread will not shut down yet
    global score
    n_beams = len(lasers)
    sequence = [random.randint(0,n_beams-1) for _ in range(100)]
    position = 0
    while True:
        for l in lasers:
            l.set(255)
        blinking_laser = lasers[sequence[position]]
        blinking_laser.blink(0.1)

        name = str(sequence[position])
        lasikService.send("set " + name + " 0")

        done = False
        while not done:
            msg = lasikService.get()
            m = re.match("^([0-9]{1,2}) ([01])$", msg)
            if m:
                msg_laser, msg_state = m.groups()
                if msg_laser == name and msg_state == "1":
                    position = position + 1
                    score = position
                    done = True
                    flash(10, Color.GREEN)
                    print("good! {}".format(position))
                    sound_good.play()
                elif msg_laser != name and msg_state == "0":
                    position = 0
                    score = position
                    done = True
                    print("bad! {}".format(position))
                    flash(20, Color.PURPLE)
                    sound_bad.play()
                    for l in lasers:
                        l.set(0)
                    print(msg_laser)
                    print(len(lasers))
                    brokenLaser = lasers[int(msg_laser)]
                    brokenLaser.set(255)
                    brokenLaser.blink(0.5)
                    time.sleep(5)
                    brokenLaser.set(0)
                    lasikService.flushMsgQueue()

def flash(nFrames, color):
    # TODO should this be made threadsafe or smth, posting pygame events from multiple
    # threads might not be very safe
    flashEvent = pygame.event.Event(FLASHEVENT, nFrames = nFrames, color = color)
    pygame.event.post(flashEvent)

def getParmsFromArgv():
    if len(sys.argv) == 1:
        host, port = "localhost", 1337
        print("no host and port specified, using defaults {}:{}".format(host,port), file=sys.stderr)
    elif len(sys.argv) == 3:
        host, port = sys.argv[1], int(sys.argv[2])
    else:
        print("usage example: '{} HOST PORT'".format(basename(sys.argv[0])), file=sys.stderr)
        exit(1)
    return host, port

def load_sound(name):
    main_dir = os.path.split(os.path.abspath(__file__))[0]
    sound_dir = os.path.join(main_dir, 'sound')
    fullname = os.path.join(sound_dir, name)
    try:
        sound = pygame.mixer.Sound(fullname)
    except pygame.error:
        print ('Cannot load sound: %s' % fullname)
        raise SystemExit(str(geterror()))
    return sound

def setupSound():
    pygame.mixer.pre_init(22050, -16, 2, 256)
    pygame.mixer.init()

    good = load_sound("smb_coin.wav")
    good.set_volume(.8)

    bad = load_sound("smb_bowserfalls.wav")
    bad.set_volume(.8)
    return good, bad

def setupDisplay():
    os.environ["DISPLAY"] = ":0"
    pygame.mouse.set_visible(False)
    #screen = pygame.display.set_mode((0,0), FULLSCREEN)
    screen = pygame.display.set_mode((700,400))
    sw, sh = screen.get_size()

    pygame.font.init()
    font = pygame.font.SysFont('Comic Sans MS', sh // 3)
    return screen, font

def queryAmountOfAvailableBeams():
    lasikService.send("get all")
    while True:
        msg = lasikService.get()
        m = re.match("^all ([01]+)$", msg)
        if m:
            n_beams = len(m.group(1))
            break
    print("detected {} beams".format(n_beams))
    return n_beams

def initAllLasers():
    n_beams = queryAmountOfAvailableBeams()
    lasers = [Laser(str(i)) for i in range(n_beams)]
    return lasers

FLASHEVENT = USEREVENT + 1

def main():
    # TODO
    global serverSocket
    global sound_good, sound_bad
    global screen, font
    global lasikService
    global lasers

    host, port = getParmsFromArgv()
    lasikService = LasikService()

    sound_good, sound_bad = setupSound() # must be called before pygame.init()
    pygame.init()
    screen, font = setupDisplay()

    lasers = initAllLasers()

    threading.Thread(target = t_consumer).start()

    clock = pygame.time.Clock()

    flashing = (0, Color.BLACK) # nframes, color
    while True:
        clock.tick(60)

        for e in pygame.event.get():
            if (e.type == QUIT or (e.type == KEYDOWN and e.key == K_ESCAPE)):
                for l in lasers:
                    l.close()
                lasikService.close()
                pygame.quit()
                exit()
            elif e.type == FLASHEVENT:
                flashing = e.nFrames, e.color

        if flashing[0]:
            nFrames, color = flashing
            nFrames -= 1
            flashing = nFrames, color
            bgColor = Color.BLACK if (nFrames // 2) % 2 else color
        else:
            bgColor = Color.BLACK

        screen.fill(bgColor)

        renderedtext = font.render(str(score), False, Color.CYAN)
        w,h = font.size(str(score))
        sw, sh = screen.get_size()
        screen.blit(renderedtext, (sw/2 - w/2, sh/2 - h/2))

        pygame.display.flip()

        pygame.event.pump()

if __name__ == "__main__":
    main()
