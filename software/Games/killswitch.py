#!/usr/bin/env python3
from threading import Thread

class Killswitch:
    def __init__(self, debug = None):
        self.killkillkill = False
        self.debug = debug if debug else lambda msg: None

    def set(self):
        if not self.killkillkill:
            self.debug("setting the killswitch")
            self.killkillkill = True

    def is_set(self):
        return self.killkillkill

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        if not exception_type == None:
            self.set()

    def looped_thread(self, name, debug):
        def decorator(work):
            def _t():
                debug("Starting thread: {}".format(name))
                with self as k:
                    while not k.is_set():
                        work(); # must timeout now and then!
                debug("Stopping thread: {}".format(name))
            return Thread(target = _t, name = name)
        return decorator
