#!/usr/bin/env python3

import sched
import threading
import time

from killswitch import Killswitch

from collections import namedtuple

# ==================================================================================================

Event = namedtuple("Event", "etype data")

# ==================================================================================================

class EventLoop:
    def __init__(self, killswitch, timeout = 1.0, debugHandler = None):
        self.s = sched.scheduler()
        self.killswitch = killswitch
        self.timeout = timeout
        self.debugHandler = debugHandler
        self.debugLock = threading.Lock()
        self.handlers = {}
        self.wakeUp = threading.Event() # for signalling a new entry to the scheduler thread
        self.hideEvents = set()

    def hideEvent(self, name):
        self.hideEvents.add(name)

    def post(self, event, delay = 0):
        self.s.enter(delay, 0, self._handleEvent, (event,))
        self.wakeUp.set() # wake up the schedule thread, it needs to recheck deadlines

    def run(self, tickPeriod = 0.010):
        threading.Thread(target = self._t_tick, args = (tickPeriod,), name = "_t_tick").start()
        threading.Thread(target = self._t_eventloop, name = "_t_eventloop").start()

    def _handleEvent(self, event):
        etype, data = event
        if etype in self.handlers:
            for h in self.handlers[etype]:
                h(event, self)

        if not etype in self.hideEvents:
            self._debug("{}".format(event))

    def _debug(self, msg):
        if self.debugHandler:
            with self.debugLock:
                self.debugHandler(msg)

    def _t_eventloop(self):
        self._debug("starting the eventloop thread")
        with self.killswitch as k:
            while not k.is_set():
                # clear the wakeUp Event as we're going to update the deadline
                self.wakeUp.clear()
                # do due work and get next deadline
                deadline = self.s.run(blocking = False)
                if deadline:
                    sleepytime = min(self.timeout, deadline)
                else:
                    sleepytime = self.timeout
                # sleep at most timeout or until next event or until someone posts a new event
                self.wakeUp.wait(timeout = sleepytime)
        self._debug("eventloop thread exited")

    def _t_tick(self, period):
        self._debug("starting tick thread with period {}s".format(period))
        with self.killswitch as k:
            while not k.is_set():
                tick = Event(etype = "tick", data = None)
                self.post(tick, 0)
                time.sleep(period)
        self._debug("tick thread exited")

    def registerHandler(self, etype, handler):
        self._debug('registered handler for "{}" using the forEvent decorator'.format(etype))
        self.handlers.setdefault(etype, set()).add(handler)

# ==================================================================================================

def demo():
    killswitch = Killswitch()
    el = EventLoop(killswitch, debugHandler = print)

    def tick_handler(event, el):
        click = Event("click", None)
        el.post(click)

    def click_handler(event, el):
        print("click")
        clack = Event("clack", None)
        el.post(clack, 0.3)

    def clack_handler(event, el):
        print("        clack")

    el.registerHandler("tick", tick_handler)
    el.registerHandler("click", click_handler)
    el.registerHandler("clack", clack_handler)

    el.run(tickPeriod = 1.0)

    print("main thread going to sleep")
    with killswitch:
        while not killswitch.is_set():
            time.sleep(1)

# ==================================================================================================

if __name__ == "__main__":
    demo()
