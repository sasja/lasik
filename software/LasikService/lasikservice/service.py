# Note: if you get 'ATE E0' messages you may want to disable ModemManager:
#       on ubuntu: 'systemctl stop ModemManager.service'

import time
import re
import threading
import glob
import sys
import signal
from os.path import basename

import serial
from serial.threaded import ReaderThread

from . import serialconnection

from . import sserver

# ==================================================================================================

def threadLocked(f):
    """decorator to make function thread exclusive"""
    lock = threading.Lock()
    def wrapper(*args, **kwargs):
        with lock:
            result = f(*args, **kwargs)
        return result
    return wrapper

# ==================================================================================================

@threadLocked
def log(msg):
    print(" -- {}".format(msg), file=sys.stderr)

@threadLocked
def output(msg):
    print(msg)
    if server:
        server.send(msg)

shutdown = False
def requestShutdown():
    global shutdown
    log("request global shutdown")
    shutdown = True

# TODO come on, this is not the way to do it
# enable debug logging for serial module
serialconnection.debug = lambda msg: log("(lc) {}".format(msg))
# shutdown all threads when a thread within this module crashes
serialconnection.onSubThreadCrash = requestShutdown

# enable debug logging for sserver module
sserver.debug = lambda msg: log("(ss) {}".format(msg))

# ==================================================================================================

def connectToAllttyACM(timeout = 1):
    paths = glob.glob("/dev/ttyACM*")
    paths.sort()

    if not paths:
        raise ConnectionError("could not find any /dev/ttyACM*")

    result = []
    try:
        for path in paths:
            while True:
                try:
                    class Protocol(serialconnection.AbstractProtocol):
                        pass
                    Protocol.recvHandler = lambda msg, path = path: serialMsgHandler(msg, path)
                    Protocol.path = path
                    ser = serial.serial_for_url(path)
                    reader = ReaderThread(ser, Protocol)
                    reader.start()
                    transport, protocol = reader.connect()
                    break
                except serial.SerialException:
                    nSeconds = 5
                    log("failed to connect to '{}', retrying in {}s".format(path, nSeconds))
                    time.sleep(nSeconds)
            result.append(protocol)
    except BaseException as e:
        for protocol in result:
            protocol.close()
        raise e

    return result

# ==================================================================================================

def listenOnStdin(handler, timeout = 1):
    global shutdown

    def raiseTimeout(_x,_y):
        raise TimeoutError()

    #note: cant use signal but in the main thread
    signal.signal(signal.SIGALRM, raiseTimeout)

    while not shutdown:
        signal.alarm(timeout)
        try:
            for line in sys.stdin:
                handler(line.strip())
            log("received EOF")
            requestShutdown()
        except TimeoutError as e:
            pass
        finally:
            signal.alarm(0)
    log("shut down stdin listener")

# ==================================================================================================

def serialMsgHandler(msg, path):
    log("{}: {}".format(path, msg))

    m = re.match("([A-L]) ([01])$", msg)
    if m:
        laser_name, state = m.groups()
        for n, (b_protocol, b_laser_name) in enumerate(beams):
            if b_protocol.getPath() == path and b_laser_name == laser_name:
                output("{} {}".format(n, state))

# ==================================================================================================

beams = []

def command_reset():
    global lasikConns
    global beams

    beams = []

    for protocol in lasikConns:
        protocol.write_line("reset")

    for protocol in lasikConns:
        for laser_name in "ABCDEFGHIJKL":
            beams.append((protocol, laser_name))

def command_detect_beams():
    global lasikConns
    global beams

    beams = []

    for protocol in lasikConns:
        response = protocol.awaitResponse("get all", "all [01]{12}$")
        bits = re.match("all ([01]{12})$", response).group(1)
        for n,b in enumerate(bits):
            if b == "1":
                laser_name = chr(ord('A') + n)
                beams.append((protocol, laser_name))
    logMsg = "detected {} beams:".format(len(beams))
    for n, (protocol, laser_name) in enumerate(beams):
        logMsg += "\n    {:2} = {}: laser {}".format(n, protocol.getPath(), laser_name)
    log(logMsg)

def command_disable_other_lasers():
    for protocol in lasikConns:
        for l in "ABCDEFGHIJKL":
            detected = False
            for b_protocol, b_laser_name in beams:
                if l == b_laser_name and protocol.getPath() == b_protocol.getPath():
                    detected = True
                    break
            if not detected:
                protocol.write_line("set {} 0".format(l))
                # beware that this might result in a false beam detection
                protocol.write_line("conf {} 0 0 0 0 0 1000 0".format(l))

def command_version():
    for protocol in lasikConns:
        protocol.write_line("version")

def argParse_set(args):
    beamNr, pwm = map(int,args)
    if not 0 <= pwm <= 255:
        log("pwm value must be in range 0-255")
        return None
    elif not 0 <= beamNr < len(beams):
        log("beamNr must be in range 0-{}".format(len(beams)-1))
        return None
    return (beamNr,pwm)

def command_set(beamNr, pwm):
    protocol, laser_name = beams[beamNr]
    protocol.write_line("set {} {}".format(laser_name, pwm))

def argParse_conf(args):
    (
        beamNr, nPre, preLength,
        nPulses, pulseLength, tolerance,
        beamcheckInterval, debounce
    ) = map(int,args)
    #TODO parameter checking
    if not 0 <= beamNr < len(beams):
        log("beamNr must be in range 0-{}".format(len(beams)-1))
        return None
    return (
        beamNr, nPre, preLength,
        nPulses, pulseLength, tolerance,
        beamcheckInterval, debounce)

def command_conf(
        beamNr, nPre, preLength,
        nPulses, pulseLength, tolerance,
        beamcheckInterval, debounce
        ):
    protocol, laser_name = beams[beamNr]
    protocol.write_line("conf {} {} {} {} {} {} {} {}".format(
        laser_name, nPre, preLength,
        nPulses, pulseLength, tolerance,
        beamcheckInterval, debounce))

def argParse_set_all(args):
    pwm, = map(int, args)
    if not 0 <= pwm <= 255:
        log("pwm value must be in range 0-255")
        return None
    return (pwm,)

def command_set_all(pwm):
    for protocol, laser_name in beams:
        protocol.write_line("set {} {}".format(laser_name, pwm))

def argParse_conf_all(args):
    #TODO parameter checking
    return map(int, args)

def command_conf_all(
        nPre, preLength,
        nPulses, pulseLength, tolerance,
        beamcheckInterval, debounce):
    for protocol, laser_name in beams:
        protocol.write_line("conf {} {} {} {} {} {} {} {}".format(
            laser_name, nPre, preLength,
            nPulses, pulseLength, tolerance,
            beamcheckInterval, debounce))

def command_get_all():
    global lasikConns
    global beams

    allBeamStates = {}

    for protocol in lasikConns:
        response = protocol.awaitResponse("get all", "all [01]{12}$")
        bits = re.match("all ([01]{12})$", response).group(1)
        for n,b in enumerate(bits):
            laser_name = chr(ord('A') + n)
            allBeamStates[(protocol.getPath(), laser_name)] = b

    bits = [allBeamStates[(protocol.getPath(), laser_name)] for protocol, laser_name in beams]

    output("all " + "".join(bits))

# ==================================================================================================

commandLock = threading.Lock()
def userCommandHandler(msg):
    # the argParse functions must take a tuple of strings and either return a tuple of valid
    # parameters its command_ or either return None if no valid parameters can be parsed from
    # the provided strings
    handlers = [
            ("set ([0-9]{1,3}) ([0-9]{1,3})$", argParse_set,        command_set),
            ("set all ([0-9]{1,3})$",          argParse_set_all,    command_set_all),
            ("conf ([0-9]{1,3})" + 7*" ([0-9]{1,4})" + "$",
                argParse_conf, command_conf),
            ("conf all" + 7*" ([0-9]{1,4})" + "$",
                argParse_conf_all,   command_conf_all),
            ("reset$",                         lambda x:x,          command_reset),
            ("detect beams$|db$",              lambda x:x,          command_detect_beams),
            ("disable other lasers$|dol$",     lambda x:x,          command_disable_other_lasers),
            ("get all$",                       lambda x:x,          command_get_all),
            ("version$",                       lambda x:x,          command_version) ]

    success = False

    for regex, argParse, command in handlers:
        m = re.match(regex, msg)
        if m:
            args = argParse(m.groups())
            if args != None: # no arguments is (), thus != None
                with commandLock:
                    command(*args)
                success = True
            break

    if not success:
        log("bad command: '{}'".format(msg))

# ==================================================================================================

def init_lasiks():
    def delay():
        t = 0.1
        log("waiting for {}s".format(t))
        time.sleep(t)

    log("sending newline")
    for protocol in lasikConns:
        protocol.write_line("")
    delay()

    log("requesting firmware version number")
    command_version()
    delay()

    log("resetting all lasers")
    command_reset()
    delay()

    log("detecting beams")
    command_detect_beams()

    log("disabling the lasers that were not detected")
    command_disable_other_lasers()

# ==================================================================================================

lasikConns = []
server = None

def main():
    global lasikConns
    global server

    if len(sys.argv) == 1:
        host, port = "localhost", 1337
        log("no host and port specified, using defaults {}:{}".format(host,port))
    elif len(sys.argv) == 3:
        host, port = sys.argv[1], int(sys.argv[2])
    else:
        log("usage example: '{} HOST PORT'".format(basename(sys.argv[0])))
        exit(1)

    try: # TODO fix error handling
        # establish connections with lasik boards
        # be aware that this creates subthreads
        log("detecting and connecting to lasiks")
        lasikConns = connectToAllttyACM()

        log("initialising lasiks")
        init_lasiks()

        log("starting socket server")
        server = sserver.SocketServer((host, port), userCommandHandler) # TODO use context manager
        server.start()

        log("starting listening on stdin")
        listenOnStdin(userCommandHandler) # this blocks until EOF

    finally:
        requestShutdown()
        if lasikConns:
            for protocol in lasikConns:
                protocol.close()

if __name__ == "__main__":
    main()
