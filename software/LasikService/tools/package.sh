#!/usr/bin/env bash

echo run this script from the package root as \'tools/package.sh\'

# convenience script to package and publish to PyPI
# make sure you have a virtualenv in env/
# and make sure you have set a new version nr in setup.py

set -e

source env/bin/activate
rm dist/*
python setup.py sdist bdist_wheel
twine upload dist/*
