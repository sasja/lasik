[![PyPI version](https://badge.fury.io/py/lasikservice.svg)](https://badge.fury.io/py/lasikservice)

Lasik service that exposes an interface to all beams of all connected lasik boards

# Usage
* to start the server: `lasikservice <host port>`
* to talk to a single board: `lasikconnect <serialdevice>` eg `lasikconnect /dev/ttyACM0`
* NOTE: pip might install these commands in `~/.local/bin` which might not be in your path.

# Installation
tested on Ubuntu 18.04 only

## From PyPI
```
pip3 install lasikservice
```

## From the gitlab repo for development
Clone the repo and in this package folder (the one with `setup.py`) run:
```
pip3 install virtualenv
virtualenv env
source env/bin/activate
pip install -e .
```
The `-e` switch installs with symlinks to this folder which is great during development.
the commands will now only be available from this virtualenv

Alternatively if you don't want/need the virtualenv stuff, just do
```
pip3 install -e .
```
or even just run the scripts directly as
```
python3 -m lasikservice.service <host> <port>
python3 -m lasikservice.serialconnection <serialdevice>
python3 -m lasikservice.socketserver <host> <port>
```

# Usage after installation with pip
* to start the server: `lasikservice <host> <port>`
* to talk to a single board: `lasikconnect <serialdevice>` eg `lasikconnect /dev/ttyACM0`
* NOTE: pip might install these commands in `~/.local/bin` which might not be in your path.
