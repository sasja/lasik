#include <vector>
#include "version.h"

static const int ledpin = PC13;

static const int n_lasers = 12;
static const int laserpins[n_lasers] = {
    PB1, PB0, PA7, PA6, PA3, PB6, PA2, PB7, PA1, PB8, PA0, PB9
    };

// detector pin order doesnt matter since any detector may detect any laser
static const int n_detectors = 12;
static const int detectorpins[n_detectors] = {
    PB13, PB12, PB15, PB11, PB14, PB10, PA9, PA8, PB3, PB4, PA10, PA15
    };

// =============================================================================

// need to detect beam n times in a row on same single sensor to match
#define SENSOR_MATCH_N 20
#define MAX_PWM 180 // to prevent overheating

// =============================================================================

#define INBUF_SIZE 1024
struct inbuf {
    char *data;
    int size;
    int pos;
    bool ready;
};

#define COMMANDBUF_SIZE 64
struct commandbuf {
    char *data;
    int size;
};

#define MAX_COMMAND_TOKENS 10

// =============================================================================

enum detection_result {
    DET_INVALID,
    DET_BEAM_BROKE,
    DET_BEAM_REMAINS_BROKEN,
    DET_BEAM_UNBROKE,
    DET_BEAM_REMAINS_UNBROKEN,
    DET_UNEXPECTED_SENSOR,
    DET_MULTIPLE_SENSORS
};

// =============================================================================

struct beamconf {
    int nPre;
    int preLength;
    int nPulses;
    int pulseLength;
    int tolerance;
    int beamcheckInterval;
    int debounce;
};

static const struct beamconf default_beamconf = {
    .nPre = 5,
    .preLength = 3,
    .nPulses = 3,
    .pulseLength = 4,
    .tolerance = 0,
    .beamcheckInterval = 25,
    .debounce = 1,
};

class LaserBeam
{
public:
    LaserBeam(char name, int pin) : name(name), pin(pin) {};

    void setPwm(char dutyCycle);
    void service();
    void configure(struct beamconf);
    bool isBlocked() {return blocked;}
    int getMatch() {return sensorMatch.current_i;}
    char getName() {return name;}
private:
    bool check();
    void reportState();
    void emitPulseTrain(int (*pulseCnt)[n_detectors]);
    enum detection_result readPulseCnt(
        int (*pulseCnt)[n_detectors],
        int *unexpected_i);
    bool debounceBeam(enum detection_result detection_result);
    void updateSensorMatch(
        enum detection_result detection_result,
        int unexpected_i);

    char name;
    int pin;

    char dutyCycle = 0;
    bool blocked = true;
    int debounce_cnt = 0;
    unsigned int nextBeamcheck = 0;

    struct
    {
        int current_i = -1;
        int new_i = -1;
        int cnt = 0;
    } sensorMatch;

    struct beamconf conf = default_beamconf;
};

void LaserBeam::setPwm(char dutyCycle)
{
    this->dutyCycle = dutyCycle;
    analogWrite(pin, this->dutyCycle);
}

void LaserBeam::service()
{
    if(millis() > nextBeamcheck)
    {
        bool changed = check();
        nextBeamcheck = millis() + conf.beamcheckInterval;
        if(changed) {
            reportState();
            digitalWrite(ledpin, !digitalRead(ledpin));
        }
    }
}

void LaserBeam::configure(struct beamconf new_conf)
{
    conf = new_conf;
    nextBeamcheck = millis() + conf.beamcheckInterval;
}

/**
 *  adds detected pulses to the passed array of counts
 *  so zero it out before passing unless you want to sum
 */
void LaserBeam::emitPulseTrain(int (*pulseCnt)[n_detectors])
{
    pinMode(pin, OUTPUT); // required after using analogWrite

    noInterrupts();
    {
        // fast pulses to bring signal to 50%
        // this should be tuned using a scope
        const int n_pre_updown = (conf.nPre + 1) / 2;
        for(int i = 0; i < n_pre_updown; i++)
        {
            delayMicroseconds(conf.preLength);
            digitalWrite(pin, HIGH);
            delayMicroseconds(conf.preLength);
            digitalWrite(pin, LOW);
        }

        bool is_on = false; // indeed last write was LOW
        for(int i = 0; i < conf.nPulses; i++)
        {
            digitalWrite(pin, is_on);
            delayMicroseconds(conf.pulseLength);
            for(int i = 0; i < n_detectors; i++)
            {
                if(digitalRead(detectorpins[i]) == is_on) (*pulseCnt)[i]++;
            }
            is_on = !is_on;
        }
    }
    interrupts();

    setPwm(dutyCycle);  // restore pwm
}

enum detection_result LaserBeam::readPulseCnt(
    int (*pulseCnt)[n_detectors],
    int *unexpected_i)
{
    int n_matches = 0;
    int triggered_sensor;

    *unexpected_i = -1;

    // count how many sensors saw the beam
    for(int i = 0; i < n_detectors; i++) {
        if((*pulseCnt)[i] >= conf.nPulses - conf.tolerance)
        {
            n_matches++;
            triggered_sensor = i;   // if only one it will be stored here
        }
    }

    if(n_matches == 0)
    {
        if(blocked == false) {
            return DET_BEAM_BROKE;
        } else {
            return DET_BEAM_REMAINS_BROKEN;
        }
    }
    else if (n_matches == 1)
    {
        if(triggered_sensor == sensorMatch.current_i)
        {
            if(blocked == true) {
                return DET_BEAM_UNBROKE;
            } else {
                return DET_BEAM_REMAINS_UNBROKEN;
            }
        }
        else                    // unexpected sensor!
        {
            *unexpected_i = triggered_sensor;
            return DET_UNEXPECTED_SENSOR;
        }
    }
    else                        // multiple sensors
    {
        return DET_MULTIPLE_SENSORS;
    }
    // TODO some assertion
}

bool LaserBeam::debounceBeam(enum detection_result detection_result)
{
    bool changed = false;

    switch(detection_result) {
        case DET_BEAM_BROKE:
        case DET_BEAM_UNBROKE:
        {
            if(debounce_cnt >= conf.debounce) {
                blocked = (detection_result == DET_BEAM_BROKE);
                debounce_cnt = 0;
                changed = true;
            } else {
                debounce_cnt++;
            }
            break;
        }
        case DET_BEAM_REMAINS_BROKEN:
        case DET_BEAM_REMAINS_UNBROKEN:
        case DET_UNEXPECTED_SENSOR:
        case DET_MULTIPLE_SENSORS:
        {
            debounce_cnt = 0;
            break;
        }
        case DET_INVALID:
        default:
        {
            // TODO some assertion
            break;
        }
    }
    return changed;
}

void LaserBeam::updateSensorMatch(
    enum detection_result detection_result,
    int unexpected_i)
{
    switch(detection_result) {
        case DET_BEAM_BROKE:
        case DET_BEAM_UNBROKE:
        case DET_BEAM_REMAINS_BROKEN:
        case DET_BEAM_REMAINS_UNBROKEN:
        case DET_MULTIPLE_SENSORS:
        {
            sensorMatch.cnt = 0;
            break;
        }
        case DET_UNEXPECTED_SENSOR:
        {
            if(unexpected_i == sensorMatch.new_i) {
                // it is the last candidate
                sensorMatch.cnt++;
            } else {
                // its a new candidate
                sensorMatch.new_i = unexpected_i;
                sensorMatch.cnt = 1;
            }

            if(sensorMatch.cnt >= SENSOR_MATCH_N) {
                //change to new sensor;
                sensorMatch.current_i = sensorMatch.new_i;
                Serial.print("info laser ");
                Serial.print(name);
                Serial.print(" <-> sensor ");
                Serial.print(sensorMatch.current_i);
                Serial.print("\n");
            }
            break;
        }
        case DET_INVALID:
        default:
        {
            // TODO some assertion
            break;
        }
    }
}

bool LaserBeam::check()
{
    int pulseCnt[n_detectors] = {0};
    emitPulseTrain(&pulseCnt);

    int unexpected_i;
    enum detection_result detection_result =
        readPulseCnt(&pulseCnt, &unexpected_i);

    bool changed = debounceBeam(detection_result);
    updateSensorMatch(detection_result, unexpected_i);

    return changed;
}

void LaserBeam::reportState()
{
    Serial.print(name);
    Serial.print(' ');
    Serial.print(blocked ? '0' : '1');
    Serial.print('\n');
}

// =============================================================================

void pollserial(struct inbuf *inbuf)
{
    char inChar;
    while(Serial.available())
    {
        inChar = Serial.read();
        if(inbuf->pos < inbuf->size - 1)
        {
            inbuf->data[inbuf->pos++] = inChar;
            if(inChar == '\n')
            {
                inbuf->ready = true;
            }
        }
        else if(inbuf->pos == inbuf->size - 1)
        {
            Serial.print("error inbuff_ovf\n");
            inbuf->data[inbuf->pos++] = '\n';
            inbuf->ready = true;
        }
        else
        {
            // overflow has already been reported
        }
    }
}

void popCommand(struct inbuf *inbuf, struct commandbuf *commandbuf)
{
    int i = 0;
    while(inbuf->data[i] != '\n')
    {
        commandbuf->data[i] = inbuf->data[i];
        i++;
    }
    commandbuf->data[i++] = '\0';

    int i2 = 0;
    inbuf->ready = false;
    while(i < inbuf->pos)
    {
        inbuf->ready = inbuf->ready || (inbuf->data[i] == '\n');
        inbuf->data[i2++] = inbuf->data[i++];
    }
    inbuf->pos = i2;
}

// =============================================================================

bool selectBeamsFromToken(
    char * token,
    std::vector<LaserBeam> *from,
    std::vector<LaserBeam*> *into)
{
    int result = 0;
    if(strncmp("all", token, 4) == 0)
    {
        for(auto&& beam : *from)
        {
            into->push_back(&beam);
        }
    }
    else if(strlen(token) == 1 &&
            token[0] >= 'A' &&
            token[0] < 'A' + n_lasers)
    {
        int index = token[0] - 'A';
        into->push_back(&(*from)[index]);
    }
    else    // malformed
    {
        result = 1;
    }

    return result;
}

int cmd_set(char * t_beam, char * t_pwm, std::vector<LaserBeam> *laserBeams)
{
    std::vector<LaserBeam*> beams = {};

    int result = selectBeamsFromToken(t_beam, laserBeams, &beams);
    if(result) return result;

    int dutyCycle = atoi(t_pwm);    // TODO check if fails
    if(dutyCycle < 0 || dutyCycle > 255) return 1;

    // TODO find better place for this
    dutyCycle = dutyCycle * MAX_PWM / 255;

    for(auto&& laserBeam : beams)
    {
        laserBeam->setPwm((char)dutyCycle);
    }
    return 0;
}

int cmd_get_all(std::vector<LaserBeam> *laserBeams)
{
    Serial.print("all ");
    for(auto&& laserBeam : *laserBeams)
    {
        Serial.print(laserBeam.isBlocked() ? '0' : '1');
    }
    Serial.print("\n");
    return 0;
}

int cmd_conf(char * t_beam,
             char * t_nPre,
             char * t_preLength,
             char * t_nPulses,
             char * t_pulseLength,
             char * t_tolerance,
             char * t_beamcheckInterval,
             char * t_debounce,
             std::vector<LaserBeam> *laserBeams)
{
    std::vector<LaserBeam*> beams = {};

    int result = selectBeamsFromToken(t_beam, laserBeams, &beams);
    if(result) return result;

    struct beamconf conf;
    // TODO check for failed atoi conversion
    conf.nPre =                 atoi(t_nPre);
    conf.preLength =            atoi(t_preLength);
    conf.nPulses =              atoi(t_nPulses);
    conf.pulseLength =          atoi(t_pulseLength);
    conf.tolerance =            atoi(t_tolerance);
    conf.beamcheckInterval =    atoi(t_beamcheckInterval);
    conf.debounce =             atoi(t_debounce);

    for(auto&& laserBeam : beams)
    {
        laserBeam->configure(conf);
    }
    return 0;
}

int cmd_reset(std::vector<LaserBeam> *laserBeams)
{
    for(auto&& laserBeam : *laserBeams)
    {
        laserBeam.configure(default_beamconf);
        laserBeam.setPwm(0);
    }
    return 0;
}

int cmd_get_match(std::vector<LaserBeam> *laserBeams)
{
    Serial.print("match");
    for(auto&& laserBeam : *laserBeams)
    {
        Serial.print(" ");
        Serial.print(laserBeam.getName());
        Serial.print(":");
        Serial.print(laserBeam.getMatch());
    }
    Serial.print("\n");
    return 0;
}

int cmd_version()
{
    Serial.print(FIRMWARE_VERSION_STRING);
    Serial.print("\n");
    return 0;
}

// =============================================================================

int matchAndDelegate(char * tokens[], int n_tokens,
                     std::vector<LaserBeam> *laserBeams)
{
    if(n_tokens == 3 && strncmp("set", tokens[0], 4) == 0)
    {
        return cmd_set(tokens[1], tokens[2], laserBeams);
    }
    else if(n_tokens == 2 &&
            strncmp("get", tokens[0], 4) == 0 &&
            strncmp("all", tokens[1], 4) == 0)
    {
        return cmd_get_all(laserBeams);
    }
    else if(n_tokens == 9 &&
            strncmp("conf", tokens[0], 5) == 0)
    {
        return cmd_conf( tokens[1], tokens[2], tokens[3], tokens[4],
                         tokens[5], tokens[6], tokens[7], tokens[8],
                         laserBeams);
    }
    else if(n_tokens == 1 &&
            strncmp("reset", tokens[0], 6) == 0)
    {
        return cmd_reset(laserBeams);
    }
    else if(n_tokens == 2 &&
            strncmp("get", tokens[0], 4) == 0 &&
            strncmp("match", tokens[1], 6) == 0)
    {
        return cmd_get_match(laserBeams);
    }
    else if(n_tokens == 1 &&
            strncmp("version", tokens[0], 8) == 0)
    {
        return cmd_version();
    }
    else
    {
        return 1;
    }
}

void handleCommand(char *command, std::vector<LaserBeam> *laserBeams)
{
    // break the command down into tokens (destructive)
    char * tokens[MAX_COMMAND_TOKENS];
    int n_tokens = 0;
    {
        char * tok;
        tok = strtok(command, " ");
        while(tok != NULL && n_tokens < MAX_COMMAND_TOKENS)
        {
            tokens[n_tokens] = tok;
            n_tokens++;
            tok = strtok(NULL, " ");
        }
    }

    // try to match the command and delegate the action
    int result = matchAndDelegate(tokens, n_tokens, laserBeams);

    if(result != 0) // bad command -> echo command with error msg
    {
        Serial.print("error cmd \"");
        for(int i = 0; i < n_tokens; i++)
        {
            Serial.print(tokens[i]);
            if(i < n_tokens - 1) Serial.print(" ");
        }
        Serial.print("\"\n");
    }
}

// =============================================================================

void initBeams(std::vector<LaserBeam> *laserBeams)
{
    for(int i = 0; i < n_lasers; i++)
    {
        laserBeams->push_back(LaserBeam('A' + i, laserpins[i]));
        pinMode(laserpins[i], OUTPUT);
    }

    for(int i = 0; i < n_detectors; i++)
    {
        pinMode(detectorpins[i], INPUT);
    }
}

void initBuffers(struct inbuf * inbuf, struct commandbuf * commandbuf)
{
    *inbuf = {
        .data = (char*) malloc(INBUF_SIZE),
        .size = INBUF_SIZE,
        .pos = 0,
        .ready = false
    };
    inbuf->data[0] = '\0';

    *commandbuf = {
        .data = (char*) malloc(COMMANDBUF_SIZE),
        .size = COMMANDBUF_SIZE,
    };
    commandbuf->data[0] = '\0';
}

// =============================================================================

int main(void)
{
    init(); // arduino initialisation stuff

    std::vector<LaserBeam> laserBeams;
    initBeams(&laserBeams);

    pinMode(ledpin, OUTPUT);

    struct inbuf inbuf;
    struct commandbuf commandbuf;
    initBuffers(&inbuf, &commandbuf);

    for(;;) {
        for(auto&& laserBeam : laserBeams)
        {
            laserBeam.service();
        }

        pollserial(&inbuf);
        if(inbuf.ready)
        {
            popCommand(&inbuf, &commandbuf);
            handleCommand(commandbuf.data, &laserBeams);
        }
    }

    return 0;
}
