EESchema Schematic File Version 4
LIBS:lasik-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 41
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1100 5450 0    60   Input ~ 0
+sensor1
Text HLabel 1100 5550 0    60   Input ~ 0
-sensor1
Text HLabel 1100 5250 0    60   Input ~ 0
+sensor2
Text HLabel 1100 5350 0    60   Input ~ 0
-sensor2
Text HLabel 1100 5050 0    60   Input ~ 0
+sensor3
Text HLabel 1100 5150 0    60   Input ~ 0
-sensor3
Text HLabel 1100 4850 0    60   Input ~ 0
+sensor4
Text HLabel 1100 4950 0    60   Input ~ 0
-sensor4
Text HLabel 1100 4650 0    60   Input ~ 0
+sensor5
Text HLabel 1100 4750 0    60   Input ~ 0
-sensor5
Text HLabel 1100 4450 0    60   Input ~ 0
+sensor6
Text HLabel 1100 4550 0    60   Input ~ 0
-sensor6
Text HLabel 1100 4150 0    60   Input ~ 0
+sensor7
Text HLabel 1100 4250 0    60   Input ~ 0
-sensor7
Text HLabel 1100 3950 0    60   Input ~ 0
+sensor8
Text HLabel 1100 4050 0    60   Input ~ 0
-sensor8
Text HLabel 1100 3750 0    60   Input ~ 0
+sensor9
Text HLabel 1100 3850 0    60   Input ~ 0
-sensor9
Text HLabel 1100 3550 0    60   Input ~ 0
+sensor10
Text HLabel 1100 3650 0    60   Input ~ 0
-sensor10
Text HLabel 1100 3350 0    60   Input ~ 0
+sensor11
Text HLabel 1100 3450 0    60   Input ~ 0
-sensor11
Text HLabel 1100 3150 0    60   Input ~ 0
+sensor12
Text HLabel 1100 3250 0    60   Input ~ 0
-sensor12
$Sheet
S 1550 2400 1600 3550
U 5AFD0534
F0 "sensor_preamp12" 60
F1 "sensor_preamp12.sch" 60
F2 "+sensor1" I L 1550 3150 60 
F3 "-sensor1" I L 1550 3250 60 
F4 "+sensor2" I L 1550 3350 60 
F5 "-sensor2" I L 1550 3450 60 
F6 "+sensor3" I L 1550 3550 60 
F7 "-sensor3" I L 1550 3650 60 
F8 "+sensor4" I L 1550 3750 60 
F9 "-sensor4" I L 1550 3850 60 
F10 "+sensor5" I L 1550 3950 60 
F11 "-sensor5" I L 1550 4050 60 
F12 "+sensor6" I L 1550 4150 60 
F13 "-sensor6" I L 1550 4250 60 
F14 "+sensor7" I L 1550 4450 60 
F15 "-sensor7" I L 1550 4550 60 
F16 "+sensor8" I L 1550 4650 60 
F17 "-sensor8" I L 1550 4750 60 
F18 "+sensor9" I L 1550 4850 60 
F19 "-sensor9" I L 1550 4950 60 
F20 "+sensor10" I L 1550 5050 60 
F21 "-sensor10" I L 1550 5150 60 
F22 "+sensor11" I L 1550 5250 60 
F23 "-sensor11" I L 1550 5350 60 
F24 "+sensor12" I L 1550 5450 60 
F25 "-sensor12" I L 1550 5550 60 
F26 "amplified_sensor1" I R 3150 5500 60 
F27 "amplified_sensor2" I R 3150 5100 60 
F28 "amplified_sensor3" I R 3150 5300 60 
F29 "amplified_sensor4" I R 3150 4900 60 
F30 "amplified_sensor5" I R 3150 4500 60 
F31 "amplified_sensor6" I R 3150 4700 60 
F32 "amplified_sensor7" I R 3150 4200 60 
F33 "amplified_sensor8" I R 3150 3800 60 
F34 "amplified_sensor9" I R 3150 4000 60 
F35 "amplified_sensor10" I R 3150 3600 60 
F36 "amplified_sensor11" I R 3150 3200 60 
F37 "amplified_sensor12" I R 3150 3400 60 
$EndSheet
$Sheet
S 3950 3000 1900 2700
U 5AFFB716
F0 "otto" 60
F1 "otto.sch" 60
F2 "amplified_sensor1" I L 3950 3800 60 
F3 "amplified_sensor2" I L 3950 3600 60 
F4 "amplified_sensor3" I L 3950 3400 60 
F5 "amplified_sensor4" I L 3950 3200 60 
F6 "amplified_sensor5" I L 3950 4500 60 
F7 "amplified_sensor6" I L 3950 4700 60 
F8 "amplified_sensor7" I L 3950 4200 60 
F9 "amplified_sensor8" I L 3950 4000 60 
F10 "amplified_sensor9" I L 3950 5300 60 
F11 "amplified_sensor10" I L 3950 5500 60 
F12 "amplified_sensor11" I L 3950 5100 60 
F13 "amplified_sensor12" I L 3950 4900 60 
F14 "triggered_out1" I R 5850 3200 60 
F15 "triggered_out2" I R 5850 3400 60 
F16 "triggered_out3" I R 5850 3600 60 
F17 "triggered_out4" I R 5850 3800 60 
F18 "triggered_out5" I R 5850 4000 60 
F19 "triggered_out6" I R 5850 4200 60 
F20 "triggered_out7" I R 5850 4500 60 
F21 "triggered_out8" I R 5850 4700 60 
F22 "triggered_out9" I R 5850 4900 60 
F23 "triggered_out10" I R 5850 5100 60 
F24 "triggered_out11" I R 5850 5300 60 
F25 "triggered_out12" I R 5850 5500 60 
$EndSheet
Text HLabel 6000 3200 2    60   Input ~ 0
triggered_out1
Text HLabel 6000 3400 2    60   Input ~ 0
triggered_out2
Text HLabel 6000 3600 2    60   Input ~ 0
triggered_out3
Text HLabel 6000 3800 2    60   Input ~ 0
triggered_out4
Text HLabel 6000 4000 2    60   Input ~ 0
triggered_out5
Text HLabel 6000 4200 2    60   Input ~ 0
triggered_out6
Text HLabel 6000 4500 2    60   Input ~ 0
triggered_out7
Text HLabel 6000 4700 2    60   Input ~ 0
triggered_out8
Text HLabel 6000 4900 2    60   Input ~ 0
triggered_out9
Text HLabel 6000 5100 2    60   Input ~ 0
triggered_out10
Text HLabel 6000 5300 2    60   Input ~ 0
triggered_out11
Text HLabel 6000 5500 2    60   Input ~ 0
triggered_out12
Text Notes 1950 2850 0    60   ~ 0
Abandon hope\nye who enter here
$Comp
L lasik-rescue:C_Small C201
U 1 1 5B063302
P 3550 3200
AR Path="/5B063302" Ref="C201"  Part="1" 
AR Path="/5AFCE970/5B063302" Ref="C201"  Part="1" 
F 0 "C201" V 3600 2950 50  0000 L CNN
F 1 "1n" V 3600 3250 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 3200 50  0001 C CNN
F 3 "" H 3550 3200 50  0001 C CNN
	1    3550 3200
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C202
U 1 1 5B064382
P 3550 3400
AR Path="/5B064382" Ref="C202"  Part="1" 
AR Path="/5AFCE970/5B064382" Ref="C202"  Part="1" 
F 0 "C202" V 3600 3150 50  0000 L CNN
F 1 "1n" V 3600 3450 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 3400 50  0001 C CNN
F 3 "" H 3550 3400 50  0001 C CNN
	1    3550 3400
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C203
U 1 1 5B064504
P 3550 3600
AR Path="/5B064504" Ref="C203"  Part="1" 
AR Path="/5AFCE970/5B064504" Ref="C203"  Part="1" 
F 0 "C203" V 3600 3350 50  0000 L CNN
F 1 "1n" V 3600 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 3600 50  0001 C CNN
F 3 "" H 3550 3600 50  0001 C CNN
	1    3550 3600
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C204
U 1 1 5B064C4F
P 3550 3800
AR Path="/5B064C4F" Ref="C204"  Part="1" 
AR Path="/5AFCE970/5B064C4F" Ref="C204"  Part="1" 
F 0 "C204" V 3600 3550 50  0000 L CNN
F 1 "1n" V 3600 3850 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 3800 50  0001 C CNN
F 3 "" H 3550 3800 50  0001 C CNN
	1    3550 3800
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C205
U 1 1 5B064DD7
P 3550 4000
AR Path="/5B064DD7" Ref="C205"  Part="1" 
AR Path="/5AFCE970/5B064DD7" Ref="C205"  Part="1" 
F 0 "C205" V 3600 3750 50  0000 L CNN
F 1 "1n" V 3600 4050 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 4000 50  0001 C CNN
F 3 "" H 3550 4000 50  0001 C CNN
	1    3550 4000
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C206
U 1 1 5B064F66
P 3550 4200
AR Path="/5B064F66" Ref="C206"  Part="1" 
AR Path="/5AFCE970/5B064F66" Ref="C206"  Part="1" 
F 0 "C206" V 3600 3950 50  0000 L CNN
F 1 "1n" V 3600 4250 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 4200 50  0001 C CNN
F 3 "" H 3550 4200 50  0001 C CNN
	1    3550 4200
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C207
U 1 1 5B0656FE
P 3550 4500
AR Path="/5B0656FE" Ref="C207"  Part="1" 
AR Path="/5AFCE970/5B0656FE" Ref="C207"  Part="1" 
F 0 "C207" V 3600 4250 50  0000 L CNN
F 1 "1n" V 3600 4550 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 4500 50  0001 C CNN
F 3 "" H 3550 4500 50  0001 C CNN
	1    3550 4500
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C208
U 1 1 5B065704
P 3550 4700
AR Path="/5B065704" Ref="C208"  Part="1" 
AR Path="/5AFCE970/5B065704" Ref="C208"  Part="1" 
F 0 "C208" V 3600 4450 50  0000 L CNN
F 1 "1n" V 3600 4750 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 4700 50  0001 C CNN
F 3 "" H 3550 4700 50  0001 C CNN
	1    3550 4700
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C209
U 1 1 5B06570A
P 3550 4900
AR Path="/5B06570A" Ref="C209"  Part="1" 
AR Path="/5AFCE970/5B06570A" Ref="C209"  Part="1" 
F 0 "C209" V 3600 4650 50  0000 L CNN
F 1 "1n" V 3600 4950 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 4900 50  0001 C CNN
F 3 "" H 3550 4900 50  0001 C CNN
	1    3550 4900
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C210
U 1 1 5B065710
P 3550 5100
AR Path="/5B065710" Ref="C210"  Part="1" 
AR Path="/5AFCE970/5B065710" Ref="C210"  Part="1" 
F 0 "C210" V 3600 4850 50  0000 L CNN
F 1 "1n" V 3600 5150 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 5100 50  0001 C CNN
F 3 "" H 3550 5100 50  0001 C CNN
	1    3550 5100
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C211
U 1 1 5B065716
P 3550 5300
AR Path="/5B065716" Ref="C211"  Part="1" 
AR Path="/5AFCE970/5B065716" Ref="C211"  Part="1" 
F 0 "C211" V 3600 5050 50  0000 L CNN
F 1 "1n" V 3600 5350 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 5300 50  0001 C CNN
F 3 "" H 3550 5300 50  0001 C CNN
	1    3550 5300
	0    -1   -1   0   
$EndComp
$Comp
L lasik-rescue:C_Small C212
U 1 1 5B06571C
P 3550 5500
AR Path="/5B06571C" Ref="C212"  Part="1" 
AR Path="/5AFCE970/5B06571C" Ref="C212"  Part="1" 
F 0 "C212" V 3600 5250 50  0000 L CNN
F 1 "1n" V 3600 5550 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3550 5500 50  0001 C CNN
F 3 "" H 3550 5500 50  0001 C CNN
	1    3550 5500
	0    -1   -1   0   
$EndComp
Text GLabel 3150 6300 0    60   Input ~ 0
amped_sense_debug
Wire Wire Line
	1550 5550 1100 5550
Wire Wire Line
	1100 5450 1550 5450
Wire Wire Line
	1550 5350 1100 5350
Wire Wire Line
	1100 5250 1550 5250
Wire Wire Line
	1550 5150 1100 5150
Wire Wire Line
	1100 5050 1550 5050
Wire Wire Line
	1550 4950 1100 4950
Wire Wire Line
	1100 4850 1550 4850
Wire Wire Line
	1550 4750 1100 4750
Wire Wire Line
	1100 4650 1550 4650
Wire Wire Line
	1550 4550 1100 4550
Wire Wire Line
	1100 4450 1550 4450
Wire Wire Line
	1100 4250 1550 4250
Wire Wire Line
	1550 4150 1100 4150
Wire Wire Line
	1100 4050 1550 4050
Wire Wire Line
	1550 3950 1100 3950
Wire Wire Line
	1100 3150 1550 3150
Wire Wire Line
	1550 3250 1100 3250
Wire Wire Line
	1100 3350 1550 3350
Wire Wire Line
	1550 3450 1100 3450
Wire Wire Line
	1100 3550 1550 3550
Wire Wire Line
	1550 3650 1100 3650
Wire Wire Line
	1100 3750 1550 3750
Wire Wire Line
	1550 3850 1100 3850
Wire Wire Line
	5850 3200 6000 3200
Wire Wire Line
	5850 3400 6000 3400
Wire Wire Line
	5850 3600 6000 3600
Wire Wire Line
	5850 3800 6000 3800
Wire Wire Line
	6000 4000 5850 4000
Wire Wire Line
	5850 4200 6000 4200
Wire Wire Line
	5850 4500 6000 4500
Wire Wire Line
	5850 4700 6000 4700
Wire Wire Line
	5850 4900 6000 4900
Wire Wire Line
	5850 5100 6000 5100
Wire Wire Line
	5850 5300 6000 5300
Wire Wire Line
	5850 5500 6000 5500
Wire Wire Line
	3650 3200 3950 3200
Wire Wire Line
	3650 3400 3950 3400
Wire Wire Line
	3650 3600 3950 3600
Wire Wire Line
	3650 3800 3950 3800
Wire Wire Line
	3650 4000 3950 4000
Wire Wire Line
	3650 4200 3950 4200
Wire Wire Line
	3650 4500 3950 4500
Wire Wire Line
	3650 4700 3950 4700
Wire Wire Line
	3650 4900 3950 4900
Wire Wire Line
	3650 5100 3950 5100
Wire Wire Line
	3650 5300 3950 5300
Wire Wire Line
	3650 5500 3950 5500
Wire Wire Line
	3150 3200 3450 3200
Wire Wire Line
	3450 3400 3150 3400
Wire Wire Line
	3150 3600 3450 3600
Wire Wire Line
	3450 3800 3150 3800
Wire Wire Line
	3150 4000 3450 4000
Wire Wire Line
	3450 4200 3150 4200
Wire Wire Line
	3150 4500 3450 4500
Wire Wire Line
	3150 4700 3450 4700
Wire Wire Line
	3450 4900 3150 4900
Wire Wire Line
	3150 5100 3450 5100
Wire Wire Line
	3150 5300 3450 5300
Wire Wire Line
	3450 5500 3250 5500
Wire Wire Line
	3150 6300 3250 6300
Wire Wire Line
	3250 6300 3250 5500
Connection ~ 3250 5500
Wire Wire Line
	3250 5500 3150 5500
$EndSCHEMATC
