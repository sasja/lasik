EESchema Schematic File Version 4
LIBS:lasik-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 40 41
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1650 4050 3    60   Input ~ 0
+laser1
Text HLabel 1550 4050 3    60   Input ~ 0
-laser1
$Comp
L lasik-rescue:Q_NMOS_GSD Q4001
U 1 1 5AFE3CEC
P 1300 4250
F 0 "Q4001" H 1050 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 800 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 1500 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 1300 4250 50  0001 L CNN
	1    1300 4250
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:R_Small R4001
U 1 1 5AFD93B1
P 1400 3850
F 0 "R4001" V 1200 3650 50  0000 L CNN
F 1 "470" V 1300 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 1400 3850 50  0001 C CNN
F 3 "" H 1400 3850 50  0001 C CNN
	1    1400 3850
	1    0    0    -1  
$EndComp
Text HLabel 1100 4200 1    60   Input ~ 0
pwm_out1
Text HLabel 2400 4050 3    60   Input ~ 0
+laser2
Text HLabel 2300 4050 3    60   Input ~ 0
-laser2
$Comp
L lasik-rescue:R_Small R4002
U 1 1 5AFDB3BF
P 2150 3850
F 0 "R4002" V 1950 3650 50  0000 L CNN
F 1 "470" V 2050 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2150 3850 50  0001 C CNN
F 3 "" H 2150 3850 50  0001 C CNN
	1    2150 3850
	1    0    0    -1  
$EndComp
Text HLabel 1850 4200 1    60   Input ~ 0
pwm_out2
Text HLabel 3150 4050 3    60   Input ~ 0
+laser3
Text HLabel 3050 4050 3    60   Input ~ 0
-laser3
$Comp
L lasik-rescue:R_Small R4003
U 1 1 5AFDB4F2
P 2900 3850
F 0 "R4003" V 2700 3650 50  0000 L CNN
F 1 "470" V 2800 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 2900 3850 50  0001 C CNN
F 3 "" H 2900 3850 50  0001 C CNN
	1    2900 3850
	1    0    0    -1  
$EndComp
Text HLabel 2600 4200 1    60   Input ~ 0
pwm_out3
Text HLabel 3900 4050 3    60   Input ~ 0
+laser4
Text HLabel 3800 4050 3    60   Input ~ 0
-laser4
$Comp
L lasik-rescue:R_Small R4004
U 1 1 5AFDB505
P 3650 3850
F 0 "R4004" V 3450 3650 50  0000 L CNN
F 1 "470" V 3550 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 3650 3850 50  0001 C CNN
F 3 "" H 3650 3850 50  0001 C CNN
	1    3650 3850
	1    0    0    -1  
$EndComp
Text HLabel 3350 4200 1    60   Input ~ 0
pwm_out4
Text HLabel 4650 4050 3    60   Input ~ 0
+laser5
Text HLabel 4550 4050 3    60   Input ~ 0
-laser5
$Comp
L lasik-rescue:R_Small R4005
U 1 1 5AFDB918
P 4400 3850
F 0 "R4005" V 4200 3650 50  0000 L CNN
F 1 "470" V 4300 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4400 3850 50  0001 C CNN
F 3 "" H 4400 3850 50  0001 C CNN
	1    4400 3850
	1    0    0    -1  
$EndComp
Text HLabel 4100 4200 1    60   Input ~ 0
pwm_out5
Text HLabel 5400 4050 3    60   Input ~ 0
+laser6
Text HLabel 5300 4050 3    60   Input ~ 0
-laser6
$Comp
L lasik-rescue:R_Small R4006
U 1 1 5AFDB92B
P 5150 3850
F 0 "R4006" V 4950 3650 50  0000 L CNN
F 1 "470" V 5050 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5150 3850 50  0001 C CNN
F 3 "" H 5150 3850 50  0001 C CNN
	1    5150 3850
	1    0    0    -1  
$EndComp
Text HLabel 4850 4200 1    60   Input ~ 0
pwm_out6
Text HLabel 6150 4050 3    60   Input ~ 0
+laser7
Text HLabel 6050 4050 3    60   Input ~ 0
-laser7
$Comp
L lasik-rescue:R_Small R4007
U 1 1 5AFDB93E
P 5900 3850
F 0 "R4007" V 5700 3650 50  0000 L CNN
F 1 "470" V 5800 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5900 3850 50  0001 C CNN
F 3 "" H 5900 3850 50  0001 C CNN
	1    5900 3850
	1    0    0    -1  
$EndComp
Text HLabel 5600 4200 1    60   Input ~ 0
pwm_out7
Text HLabel 6900 4050 3    60   Input ~ 0
+laser8
Text HLabel 6800 4050 3    60   Input ~ 0
-laser8
$Comp
L lasik-rescue:R_Small R4008
U 1 1 5AFDB951
P 6650 3850
F 0 "R4008" V 6450 3650 50  0000 L CNN
F 1 "470" V 6550 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 6650 3850 50  0001 C CNN
F 3 "" H 6650 3850 50  0001 C CNN
	1    6650 3850
	1    0    0    -1  
$EndComp
Text HLabel 6350 4200 1    60   Input ~ 0
pwm_out8
Text HLabel 7650 4050 3    60   Input ~ 0
+laser9
Text HLabel 7550 4050 3    60   Input ~ 0
-laser9
$Comp
L lasik-rescue:R_Small R4009
U 1 1 5AFDBB6C
P 7400 3850
F 0 "R4009" V 7200 3650 50  0000 L CNN
F 1 "470" V 7300 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 7400 3850 50  0001 C CNN
F 3 "" H 7400 3850 50  0001 C CNN
	1    7400 3850
	1    0    0    -1  
$EndComp
Text HLabel 7100 4200 1    60   Input ~ 0
pwm_out9
Text HLabel 8400 4050 3    60   Input ~ 0
+laser10
Text HLabel 8300 4050 3    60   Input ~ 0
-laser10
$Comp
L lasik-rescue:R_Small R4010
U 1 1 5AFDBB7F
P 8150 3850
F 0 "R4010" V 7950 3650 50  0000 L CNN
F 1 "470" V 8050 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 8150 3850 50  0001 C CNN
F 3 "" H 8150 3850 50  0001 C CNN
	1    8150 3850
	1    0    0    -1  
$EndComp
Text HLabel 7850 4200 1    60   Input ~ 0
pwm_out10
Text HLabel 9150 4050 3    60   Input ~ 0
+laser11
Text HLabel 9050 4050 3    60   Input ~ 0
-laser11
$Comp
L lasik-rescue:R_Small R4011
U 1 1 5AFDBB92
P 8900 3850
F 0 "R4011" V 8700 3650 50  0000 L CNN
F 1 "470" V 8800 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 8900 3850 50  0001 C CNN
F 3 "" H 8900 3850 50  0001 C CNN
	1    8900 3850
	1    0    0    -1  
$EndComp
Text HLabel 8600 4200 1    60   Input ~ 0
pwm_out11
Text HLabel 9900 4050 3    60   Input ~ 0
+laser12
Text HLabel 9800 4050 3    60   Input ~ 0
-laser12
$Comp
L lasik-rescue:R_Small R4012
U 1 1 5AFDBBA5
P 9650 3850
F 0 "R4012" V 9450 3650 50  0000 L CNN
F 1 "470" V 9550 3650 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9650 3850 50  0001 C CNN
F 3 "" H 9650 3850 50  0001 C CNN
	1    9650 3850
	1    0    0    -1  
$EndComp
Text HLabel 9350 4200 1    60   Input ~ 0
pwm_out12
$Comp
L power:+12V #PWR?
U 1 1 5AFDD0B2
P 5500 3500
F 0 "#PWR?" H 5500 3350 50  0001 C CNN
F 1 "+12V" H 5500 3640 50  0000 C CNN
F 2 "" H 5500 3500 50  0000 C CNN
F 3 "" H 5500 3500 50  0000 C CNN
	1    5500 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 4050 1400 4050
Wire Wire Line
	1400 3950 1650 3950
Wire Wire Line
	1100 4200 1100 4250
Wire Wire Line
	1650 3950 1650 4050
Wire Wire Line
	2300 4050 2150 4050
Wire Wire Line
	2150 3950 2400 3950
Wire Wire Line
	1850 4200 1850 4250
Wire Wire Line
	2400 3950 2400 4050
Wire Wire Line
	3050 4050 2900 4050
Wire Wire Line
	2900 3950 3150 3950
Wire Wire Line
	2600 4200 2600 4250
Wire Wire Line
	3150 3950 3150 4050
Wire Wire Line
	3800 4050 3650 4050
Wire Wire Line
	3650 3950 3900 3950
Wire Wire Line
	3350 4200 3350 4250
Wire Wire Line
	3900 3950 3900 4050
Wire Wire Line
	4550 4050 4400 4050
Wire Wire Line
	4400 3950 4650 3950
Wire Wire Line
	4100 4200 4100 4250
Wire Wire Line
	4650 3950 4650 4050
Wire Wire Line
	5300 4050 5150 4050
Wire Wire Line
	5150 3950 5400 3950
Wire Wire Line
	4850 4200 4850 4250
Wire Wire Line
	5400 3950 5400 4050
Wire Wire Line
	6050 4050 5900 4050
Wire Wire Line
	5900 3950 6150 3950
Wire Wire Line
	5600 4200 5600 4250
Wire Wire Line
	6150 3950 6150 4050
Wire Wire Line
	6800 4050 6650 4050
Wire Wire Line
	6650 3950 6900 3950
Wire Wire Line
	6350 4200 6350 4250
Wire Wire Line
	6900 3950 6900 4050
Wire Wire Line
	7550 4050 7400 4050
Wire Wire Line
	7400 3950 7650 3950
Wire Wire Line
	7100 4200 7100 4250
Wire Wire Line
	7650 3950 7650 4050
Wire Wire Line
	8300 4050 8150 4050
Wire Wire Line
	8150 3950 8400 3950
Wire Wire Line
	7850 4200 7850 4250
Wire Wire Line
	8400 3950 8400 4050
Wire Wire Line
	9050 4050 8900 4050
Wire Wire Line
	8900 3950 9150 3950
Wire Wire Line
	8600 4200 8600 4250
Wire Wire Line
	9150 3950 9150 4050
Wire Wire Line
	9800 4050 9650 4050
Wire Wire Line
	9650 3950 9900 3950
Wire Wire Line
	9350 4200 9350 4250
Wire Wire Line
	9900 3950 9900 4050
Wire Wire Line
	9650 3500 9650 3750
Wire Wire Line
	1400 3500 9650 3500
Wire Wire Line
	1400 3750 1400 3500
Connection ~ 5500 3500
Wire Wire Line
	2150 3750 2150 3500
Connection ~ 2150 3500
Wire Wire Line
	2900 3750 2900 3500
Connection ~ 2900 3500
Wire Wire Line
	3650 3750 3650 3500
Connection ~ 3650 3500
Wire Wire Line
	4400 3750 4400 3500
Connection ~ 4400 3500
Wire Wire Line
	5150 3750 5150 3500
Connection ~ 5150 3500
Wire Wire Line
	5900 3750 5900 3500
Connection ~ 5900 3500
Wire Wire Line
	6650 3750 6650 3500
Connection ~ 6650 3500
Wire Wire Line
	7400 3750 7400 3500
Connection ~ 7400 3500
Wire Wire Line
	8150 3750 8150 3500
Connection ~ 8150 3500
Wire Wire Line
	8900 3750 8900 3500
Connection ~ 8900 3500
$Comp
L lasik-rescue:GNDPWR #PWR?
U 1 1 5AFDDCE9
P 5500 4550
F 0 "#PWR?" H 5500 4350 50  0001 C CNN
F 1 "GNDPWR" H 5500 4420 50  0000 C CNN
F 2 "" H 5500 4500 50  0000 C CNN
F 3 "" H 5500 4500 50  0000 C CNN
	1    5500 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 4550 9650 4450
Wire Wire Line
	1400 4550 9650 4550
Wire Wire Line
	1400 4450 1400 4550
Connection ~ 5500 4550
Wire Wire Line
	2150 4450 2150 4550
Connection ~ 2150 4550
Wire Wire Line
	2900 4450 2900 4550
Connection ~ 2900 4550
Wire Wire Line
	3650 4450 3650 4550
Connection ~ 3650 4550
Wire Wire Line
	4400 4450 4400 4550
Connection ~ 4400 4550
Wire Wire Line
	5150 4450 5150 4550
Connection ~ 5150 4550
Wire Wire Line
	5900 4450 5900 4550
Connection ~ 5900 4550
Wire Wire Line
	6650 4450 6650 4550
Connection ~ 6650 4550
Wire Wire Line
	7400 4450 7400 4550
Connection ~ 7400 4550
Wire Wire Line
	8150 4450 8150 4550
Connection ~ 8150 4550
Wire Wire Line
	8900 4450 8900 4550
Connection ~ 8900 4550
$Comp
L lasik-rescue:Q_NMOS_GSD Q4002
U 1 1 5AFE93D9
P 2050 4250
F 0 "Q4002" H 1800 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 1550 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 2250 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 2050 4250 50  0001 L CNN
	1    2050 4250
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:Q_NMOS_GSD Q4003
U 1 1 5AFE9439
P 2800 4250
F 0 "Q4003" H 2550 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 2300 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 3000 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 2800 4250 50  0001 L CNN
	1    2800 4250
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:Q_NMOS_GSD Q4004
U 1 1 5AFE949A
P 3550 4250
F 0 "Q4004" H 3300 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 3050 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 3750 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 3550 4250 50  0001 L CNN
	1    3550 4250
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:Q_NMOS_GSD Q4005
U 1 1 5AFE94FC
P 4300 4250
F 0 "Q4005" H 4050 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 3800 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 4500 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 4300 4250 50  0001 L CNN
	1    4300 4250
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:Q_NMOS_GSD Q4006
U 1 1 5AFE9561
P 5050 4250
F 0 "Q4006" H 4800 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 4550 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 5250 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 5050 4250 50  0001 L CNN
	1    5050 4250
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:Q_NMOS_GSD Q4007
U 1 1 5AFE962B
P 5800 4250
F 0 "Q4007" H 5550 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 5300 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 6000 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 5800 4250 50  0001 L CNN
	1    5800 4250
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:Q_NMOS_GSD Q4008
U 1 1 5AFE96A2
P 6550 4250
F 0 "Q4008" H 6300 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 6050 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 6750 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 6550 4250 50  0001 L CNN
	1    6550 4250
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:Q_NMOS_GSD Q4009
U 1 1 5AFE9722
P 7300 4250
F 0 "Q4009" H 7050 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 6800 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 7500 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 7300 4250 50  0001 L CNN
	1    7300 4250
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:Q_NMOS_GSD Q4010
U 1 1 5AFE9787
P 8050 4250
F 0 "Q4010" H 7800 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 7550 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 8250 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 8050 4250 50  0001 L CNN
	1    8050 4250
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:Q_NMOS_GSD Q4011
U 1 1 5AFE97F9
P 8800 4250
F 0 "Q4011" H 8550 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 8300 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 9000 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 8800 4250 50  0001 L CNN
	1    8800 4250
	1    0    0    -1  
$EndComp
$Comp
L lasik-rescue:Q_NMOS_GSD Q4012
U 1 1 5AFE9870
P 9550 4250
F 0 "Q4012" H 9300 4100 50  0000 L CNN
F 1 "DMG2302UK-7" H 9050 4000 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 9750 4175 50  0001 L CIN
F 3 "https://www.mouser.be/datasheet/2/115/DMG2302UK-959451.pdf" H 9550 4250 50  0001 L CNN
	1    9550 4250
	1    0    0    -1  
$EndComp
Text GLabel 9900 3850 2    60   Input ~ 0
laser_current_debug
Text GLabel 9900 3600 2    60   Input ~ 0
bp_pwm_debug
Wire Wire Line
	9900 3850 9850 3850
Wire Wire Line
	9850 3850 9850 3950
Connection ~ 9850 3950
Wire Wire Line
	9300 3600 9300 4250
Wire Wire Line
	9300 4250 9350 4250
Connection ~ 9350 4250
Wire Wire Line
	9900 3600 9300 3600
$EndSCHEMATC
