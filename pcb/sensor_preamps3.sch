EESchema Schematic File Version 4
LIBS:lasik-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 41
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1450 1650 1400 700 
U 5AFE6171
F0 "sensor_preamp1" 60
F1 "sensor_preamp1.sch" 60
F2 "sense_voltage" I R 2850 2150 60 
F3 "+sensor" I L 1450 1800 60 
F4 "-sensor" I L 1450 2150 60 
F5 "sensor_amplified" I R 2850 1800 60 
$EndSheet
$Sheet
S 1450 4800 1400 300 
U 5AFE6F9E
F0 "sense_voltage" 60
F1 "sense_voltage.sch" 60
F2 "sense_voltage" I R 2850 4950 60 
$EndSheet
Text HLabel 1050 1800 0    60   Input ~ 0
+sensor1
Text HLabel 1050 2150 0    60   Input ~ 0
-sensor1
$Sheet
S 1450 2750 1400 700 
U 5B0224AC
F0 "sensor_preamp2" 60
F1 "sensor_preamp1.sch" 60
F2 "sense_voltage" I R 2850 3250 60 
F3 "+sensor" I L 1450 2900 60 
F4 "-sensor" I L 1450 3250 60 
F5 "sensor_amplified" I R 2850 2900 60 
$EndSheet
Text HLabel 1050 2900 0    60   Input ~ 0
+sensor2
Text HLabel 1050 3250 0    60   Input ~ 0
-sensor2
$Sheet
S 1450 3800 1400 700 
U 5AFE85BA
F0 "sensor_preamp3" 60
F1 "sensor_preamp1.sch" 60
F2 "sense_voltage" I R 2850 4300 60 
F3 "+sensor" I L 1450 3950 60 
F4 "-sensor" I L 1450 4300 60 
F5 "sensor_amplified" I R 2850 3950 60 
$EndSheet
Text HLabel 1050 3950 0    60   Input ~ 0
+sensor3
Text HLabel 1050 4300 0    60   Input ~ 0
-sensor3
Wire Wire Line
	1050 3950 1450 3950
Wire Wire Line
	1050 4300 1450 4300
Wire Wire Line
	1050 3250 1450 3250
Wire Wire Line
	1450 2900 1050 2900
Wire Wire Line
	1050 1800 1450 1800
Wire Wire Line
	1050 2150 1450 2150
Wire Wire Line
	2950 4950 2850 4950
Wire Wire Line
	2950 2150 2950 3250
Wire Wire Line
	2950 4300 2850 4300
Wire Wire Line
	2950 3250 2850 3250
Connection ~ 2950 4300
Wire Wire Line
	2950 2150 2850 2150
Connection ~ 2950 3250
Text HLabel 3250 1800 2    60   Input ~ 0
sensor1_amplified
Text HLabel 3250 2900 2    60   Input ~ 0
sensor2_amplified
Text HLabel 3250 3950 2    60   Input ~ 0
sensor3_amplified
Wire Wire Line
	3250 3950 2850 3950
Wire Wire Line
	3250 2900 2850 2900
Wire Wire Line
	3250 1800 2850 1800
Wire Wire Line
	2950 4300 2950 4950
Wire Wire Line
	2950 3250 2950 4300
$EndSCHEMATC
