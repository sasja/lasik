EESchema Schematic File Version 4
LIBS:lasik-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 41
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1100 3150 0    60   Input ~ 0
+sensor1
Text HLabel 1100 3250 0    60   Input ~ 0
-sensor1
Text HLabel 1100 3350 0    60   Input ~ 0
+sensor2
Text HLabel 1100 3450 0    60   Input ~ 0
-sensor2
Text HLabel 1100 3550 0    60   Input ~ 0
+sensor3
Text HLabel 1100 3650 0    60   Input ~ 0
-sensor3
Text HLabel 1100 3750 0    60   Input ~ 0
+sensor4
Text HLabel 1100 3850 0    60   Input ~ 0
-sensor4
Text HLabel 1100 3950 0    60   Input ~ 0
+sensor5
Text HLabel 1100 4050 0    60   Input ~ 0
-sensor5
Text HLabel 1100 4150 0    60   Input ~ 0
+sensor6
Text HLabel 1100 4250 0    60   Input ~ 0
-sensor6
Text HLabel 1100 4450 0    60   Input ~ 0
+sensor7
Text HLabel 1100 4550 0    60   Input ~ 0
-sensor7
Text HLabel 1100 4650 0    60   Input ~ 0
+sensor8
Text HLabel 1100 4750 0    60   Input ~ 0
-sensor8
Text HLabel 1100 4850 0    60   Input ~ 0
+sensor9
Text HLabel 1100 4950 0    60   Input ~ 0
-sensor9
Text HLabel 1100 5050 0    60   Input ~ 0
+sensor10
Text HLabel 1100 5150 0    60   Input ~ 0
-sensor10
Text HLabel 1100 5250 0    60   Input ~ 0
+sensor11
Text HLabel 1100 5350 0    60   Input ~ 0
-sensor11
Text HLabel 1100 5450 0    60   Input ~ 0
+sensor12
Text HLabel 1100 5550 0    60   Input ~ 0
-sensor12
$Sheet
S 1800 2650 1500 700 
U 5AFE32FA
F0 "sensor_preamps3-1" 60
F1 "sensor_preamps3.sch" 60
F2 "+sensor1" I L 1800 2700 60 
F3 "-sensor1" I L 1800 2800 60 
F4 "+sensor2" I L 1800 2950 60 
F5 "-sensor2" I L 1800 3050 60 
F6 "+sensor3" I L 1800 3200 60 
F7 "-sensor3" I L 1800 3300 60 
F8 "sensor1_amplified" I R 3300 2750 60 
F9 "sensor2_amplified" I R 3300 3000 60 
F10 "sensor3_amplified" I R 3300 3250 60 
$EndSheet
$Sheet
S 1800 3550 1500 700 
U 5AFEB6FF
F0 "sensor_preamps3-2" 60
F1 "sensor_preamps3.sch" 60
F2 "+sensor1" I L 1800 3600 60 
F3 "-sensor1" I L 1800 3700 60 
F4 "+sensor2" I L 1800 3850 60 
F5 "-sensor2" I L 1800 3950 60 
F6 "+sensor3" I L 1800 4100 60 
F7 "-sensor3" I L 1800 4200 60 
F8 "sensor1_amplified" I R 3300 3650 60 
F9 "sensor2_amplified" I R 3300 3900 60 
F10 "sensor3_amplified" I R 3300 4150 60 
$EndSheet
$Sheet
S 1800 4450 1500 700 
U 5AFEBD9B
F0 "sensor_preamps3-3" 60
F1 "sensor_preamps3.sch" 60
F2 "+sensor1" I L 1800 4500 60 
F3 "-sensor1" I L 1800 4600 60 
F4 "+sensor2" I L 1800 4750 60 
F5 "-sensor2" I L 1800 4850 60 
F6 "+sensor3" I L 1800 5000 60 
F7 "-sensor3" I L 1800 5100 60 
F8 "sensor1_amplified" I R 3300 4550 60 
F9 "sensor2_amplified" I R 3300 4800 60 
F10 "sensor3_amplified" I R 3300 5050 60 
$EndSheet
$Sheet
S 1800 5350 1500 700 
U 5AFF09EF
F0 "sensor_preamps3-4" 60
F1 "sensor_preamps3.sch" 60
F2 "+sensor1" I L 1800 5400 60 
F3 "-sensor1" I L 1800 5500 60 
F4 "+sensor2" I L 1800 5650 60 
F5 "-sensor2" I L 1800 5750 60 
F6 "+sensor3" I L 1800 5900 60 
F7 "-sensor3" I L 1800 6000 60 
F8 "sensor1_amplified" I R 3300 5450 60 
F9 "sensor2_amplified" I R 3300 5700 60 
F10 "sensor3_amplified" I R 3300 5950 60 
$EndSheet
Wire Wire Line
	1800 2700 1100 2700
Wire Wire Line
	1100 2700 1100 3150
Wire Wire Line
	1800 2800 1150 2800
Wire Wire Line
	1150 2800 1150 3250
Wire Wire Line
	1150 3250 1100 3250
Wire Wire Line
	1800 2950 1200 2950
Wire Wire Line
	1200 2950 1200 3350
Wire Wire Line
	1200 3350 1100 3350
Wire Wire Line
	1250 3050 1250 3450
Wire Wire Line
	1250 3450 1100 3450
Wire Wire Line
	1800 3200 1300 3200
Wire Wire Line
	1300 3200 1300 3550
Wire Wire Line
	1300 3550 1100 3550
Wire Wire Line
	1800 3300 1350 3300
Wire Wire Line
	1350 3300 1350 3650
Wire Wire Line
	1350 3650 1100 3650
Wire Wire Line
	1800 3600 1400 3600
Wire Wire Line
	1400 3600 1400 3750
Wire Wire Line
	1400 3750 1100 3750
Wire Wire Line
	1800 3700 1450 3700
Wire Wire Line
	1450 3700 1450 3850
Wire Wire Line
	1450 3850 1100 3850
Wire Wire Line
	1800 3850 1500 3850
Wire Wire Line
	1500 3850 1500 3950
Wire Wire Line
	1500 3950 1100 3950
Wire Wire Line
	1800 3950 1550 3950
Wire Wire Line
	1550 3950 1550 4050
Wire Wire Line
	1550 4050 1100 4050
Wire Wire Line
	1800 4100 1100 4100
Wire Wire Line
	1100 4100 1100 4150
Wire Wire Line
	1800 4200 1100 4200
Wire Wire Line
	1100 4200 1100 4250
Wire Wire Line
	1800 4500 1750 4500
Wire Wire Line
	1750 4500 1750 4450
Wire Wire Line
	1750 4450 1100 4450
Wire Wire Line
	1800 4600 1750 4600
Wire Wire Line
	1750 4600 1750 4550
Wire Wire Line
	1750 4550 1100 4550
Wire Wire Line
	1800 4750 1750 4750
Wire Wire Line
	1750 4750 1750 4650
Wire Wire Line
	1750 4650 1100 4650
Wire Wire Line
	1700 4850 1800 4850
Wire Wire Line
	1700 4850 1700 4750
Wire Wire Line
	1700 4750 1100 4750
Wire Wire Line
	1800 5000 1650 5000
Wire Wire Line
	1650 5000 1650 4850
Wire Wire Line
	1650 4850 1100 4850
Wire Wire Line
	1800 5100 1600 5100
Wire Wire Line
	1600 5100 1600 4950
Wire Wire Line
	1600 4950 1100 4950
Wire Wire Line
	1800 5400 1550 5400
Wire Wire Line
	1550 5400 1550 5050
Wire Wire Line
	1550 5050 1100 5050
Wire Wire Line
	1800 5500 1500 5500
Wire Wire Line
	1500 5500 1500 5150
Wire Wire Line
	1500 5150 1100 5150
Wire Wire Line
	1800 5650 1450 5650
Wire Wire Line
	1450 5650 1450 5250
Wire Wire Line
	1450 5250 1100 5250
Wire Wire Line
	1800 5750 1400 5750
Wire Wire Line
	1400 5750 1400 5350
Wire Wire Line
	1400 5350 1100 5350
Wire Wire Line
	1800 5900 1350 5900
Wire Wire Line
	1350 5900 1350 5450
Wire Wire Line
	1350 5450 1100 5450
Wire Wire Line
	1800 6000 1300 6000
Wire Wire Line
	1300 6000 1300 5550
Wire Wire Line
	1300 5550 1100 5550
Text HLabel 3500 2750 2    60   Input ~ 0
amplified_sensor1
Text HLabel 3500 3000 2    60   Input ~ 0
amplified_sensor2
Text HLabel 3500 3250 2    60   Input ~ 0
amplified_sensor3
Text HLabel 3500 3650 2    60   Input ~ 0
amplified_sensor4
Text HLabel 3500 3900 2    60   Input ~ 0
amplified_sensor5
Text HLabel 3500 4150 2    60   Input ~ 0
amplified_sensor6
Text HLabel 3500 4550 2    60   Input ~ 0
amplified_sensor7
Text HLabel 3500 4800 2    60   Input ~ 0
amplified_sensor8
Text HLabel 3500 5050 2    60   Input ~ 0
amplified_sensor9
Text HLabel 3500 5450 2    60   Input ~ 0
amplified_sensor10
Text HLabel 3500 5700 2    60   Input ~ 0
amplified_sensor11
Text HLabel 3500 5950 2    60   Input ~ 0
amplified_sensor12
Wire Wire Line
	3300 2750 3500 2750
Wire Wire Line
	3300 3000 3500 3000
Wire Wire Line
	3300 3250 3500 3250
Wire Wire Line
	3300 3650 3500 3650
Wire Wire Line
	3300 3900 3500 3900
Wire Wire Line
	3300 4150 3500 4150
Wire Wire Line
	3300 4550 3500 4550
Wire Wire Line
	3300 4800 3500 4800
Wire Wire Line
	3300 5050 3500 5050
Wire Wire Line
	3300 5450 3500 5450
Wire Wire Line
	3300 5700 3500 5700
Wire Wire Line
	3300 5950 3500 5950
Wire Wire Line
	1250 3050 1800 3050
$EndSCHEMATC
