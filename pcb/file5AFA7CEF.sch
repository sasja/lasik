EESchema Schematic File Version 2
LIBS:lasik-rescue
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32duino_boards
LIBS:bluepill_breakouts
LIBS:lasik-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 51
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1200 1550 0    60   Input ~ 0
sensor_1
Text HLabel 1200 1300 0    60   Input ~ 0
sensor_2
Text HLabel 1200 1050 0    60   Input ~ 0
sensor_3
$Sheet
S 1750 2800 1150 550 
U 5AFABFF4
F0 "bias_supply" 60
F1 "file5AFABFF3.sch" 60
F2 "bias" I R 2900 2950 60 
$EndSheet
$Sheet
S 2300 850  1250 1250
U 5AFABFF7
F0 "kevin_1" 60
F1 "kevin_amp.sch" 60
F2 "signal" I L 2300 1000 60 
F3 "bias" I L 2300 1950 60 
F4 "output" I R 3550 1950 60 
$EndSheet
$Sheet
S 4000 850  1250 1250
U 5AFABFFA
F0 "kevin_2" 60
F1 "kevin_amp.sch" 60
F2 "signal" I L 4000 1000 60 
F3 "bias" I L 4000 1950 60 
F4 "output" I R 5250 1950 60 
$EndSheet
$Sheet
S 5650 850  1250 1250
U 5AFABFFD
F0 "kevin_3" 60
F1 "kevin_amp.sch" 60
F2 "signal" I L 5650 1000 60 
F3 "bias" I L 5650 1950 60 
F4 "output" I R 6900 1950 60 
$EndSheet
Text HLabel 7750 1400 2    60   Input ~ 0
output_1
Text HLabel 7750 1200 2    60   Input ~ 0
output_2
Text HLabel 7750 1000 2    60   Input ~ 0
output_3
Wire Wire Line
	1200 1550 2100 1550
Wire Wire Line
	2100 1550 2100 1000
Wire Wire Line
	2100 1000 2300 1000
Wire Wire Line
	1200 1300 2000 1300
Wire Wire Line
	2000 1300 2000 700 
Wire Wire Line
	2000 700  3900 700 
Wire Wire Line
	3900 700  3900 1000
Wire Wire Line
	3900 1000 4000 1000
Wire Wire Line
	1200 1050 1900 1050
Wire Wire Line
	1900 1050 1900 600 
Wire Wire Line
	1900 600  5550 600 
Wire Wire Line
	5550 600  5550 1000
Wire Wire Line
	5550 1000 5650 1000
Wire Wire Line
	6900 1950 7000 1950
Wire Wire Line
	7000 1950 7000 1000
Wire Wire Line
	7000 1000 7750 1000
Wire Wire Line
	5250 1950 5350 1950
Wire Wire Line
	5350 1950 5350 2250
Wire Wire Line
	5350 2250 7100 2250
Wire Wire Line
	7100 2250 7100 1200
Wire Wire Line
	7100 1200 7750 1200
Wire Wire Line
	3550 1950 3650 1950
Wire Wire Line
	3650 1950 3650 2350
Wire Wire Line
	3650 2350 7200 2350
Wire Wire Line
	7200 2350 7200 1400
Wire Wire Line
	7200 1400 7750 1400
Wire Wire Line
	5650 1950 5550 1950
Wire Wire Line
	5550 1950 5550 2500
Wire Wire Line
	5550 2500 2150 2500
Wire Wire Line
	2150 2500 2150 1950
Wire Wire Line
	2150 1950 2300 1950
Wire Wire Line
	4000 1950 3900 1950
Wire Wire Line
	3900 1950 3900 2500
Connection ~ 3900 2500
Connection ~ 3200 2500
Wire Wire Line
	2900 2950 3200 2950
Wire Wire Line
	3200 2950 3200 2500
$EndSCHEMATC
