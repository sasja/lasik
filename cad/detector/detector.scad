$fn = 100;

r = 15;             // radius of the sphere
r_wire = 1.5;       // radius of the wire
border = 1.5;       // minimum material width
bottom = 15;        // length under halfdome inside

// =============================================================================

// general sensor dimensions
dim = [
    2 * r + 2 * border,
    r + border,
    2 * r + 1 * border + bottom
];

sphere_pos = [dim.x/2, 0, bottom + r];

// BPW sensor dimensions
BPW_dim = [4.3, 4.8, 2];

// a slot for the pin and wire of the detector
wire_slot_dim = [2, 2, 10];


module BPW() {
    BPW_extend = 5;

    translate([0, 0, BPW_extend/2 - BPW_dim.z/2]) {
        cube(BPW_dim + [0,0,BPW_extend], center = true);

        translate([0, wire_slot_dim.y/2, -wire_slot_dim.z/2])
        cube(wire_slot_dim + [0, BPW_dim.y, BPW_dim.z + BPW_extend], center = true);
    }
}

module detector(){
    remove = 1.55 * r + border;     // distance from top till rectangular block
    extra_sphere_angle = 14;

    block_dim = [dim.x, tan(extra_sphere_angle) * remove, remove];

    difference(){
        union(){
            cube(dim);

            // allow some extra overhanging sphere surface in front

            translate([0,0,dim.z - remove])
            rotate([extra_sphere_angle,0,0])
            cube(block_dim);
        }

        // the half sphere hole
        translate(sphere_pos) sphere(r=r);

        // the wire
        translate([sphere_pos.x - r_wire, -BPW_dim.x, -1])
        cube([2*r_wire, 2*r_wire + BPW_dim.x, bottom + 2]);

        // photodetector slot
        translate([dim.x/2, BPW_dim.x/2, sphere_pos.z - r]) BPW();

        // remove extra material at the top
        difference() {
            translate([-1,-block_dim.y - 1, dim.z - remove])
            cube([dim.x + 2, dim.y + block_dim.y + 2, remove + 1]);

            translate(sphere_pos) {
                sphere(r = r + border);
                rotate([-90,0,0]) cylinder(h=dim.y + 1, r = 0.6 * r);
            }
        }

        // finally shave some from sides
        outer_r = r + border;
        shave = outer_r - sqrt(2*outer_r*remove - remove*remove);

        translate([-1, -1, -1])
        cube([shave + 1, dim.y + 2, dim.z - remove + 1]);

        translate([dim.x + 1, -1, -1]) mirror([1,0,0])
        cube([shave + 1, dim.y + 2, dim.z - remove + 1]);
    }

}

detector();
