// sensor dimensions
detWidth = 4.8;
detDepth = 4.5;
detHeight = 2;

// =====================================================

//BPW_dim = [4.65, 4.3, 2];
BPW_leads_offset = 0.225;
BPW_leads_r = 1.7 / 2;
BPW_leads_l = 8;
BPW_leads_angle = 10;

module BPW_lead(extend)
{
    translate([-detWidth/2 - BPW_leads_offset, 0, 0])
    rotate([0,BPW_leads_angle,0])
    translate([0,0,-extend])
    //cylinder(h = BPW_leads_l + extend, r = BPW_leads_r);
    translate([-BPW_leads_r,-BPW_leads_r,0])
    cube([detWidth/3, 2*BPW_leads_r, BPW_leads_l + extend]);
}

module BPW(extend = 5) mirror([0,0,1]) union() {
    translate([-detWidth/2, -detDepth/2,-extend])
    cube([detWidth, detDepth, detHeight + extend]);

    BPW_lead(extend);
    mirror([1,0,0]) BPW_lead(extend);
}
