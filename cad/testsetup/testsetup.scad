use <array.scad>

$fn = 50;

base_depth = 8;

lasik_dim = [67.3, 67.3, 2];

base_dim = [300, 380, base_depth];
hole_dim = [200, 200, 0];

det_z = 24.5;

rLaser = 3.0625;
laser_dim = [10, 10, det_z];

module lasik() {
    translate([-lasik_dim.x/2, -lasik_dim.y/2]){
        color("green")
        cube(lasik_dim);
        color("lightgreen") {
            w = 5; h = 7;
            translate([0,0,lasik_dim.z]) cube([w, lasik_dim.y, h]);
            translate([lasik_dim.x-w,0,lasik_dim.z]) cube([w, lasik_dim.y, h]);
            translate([30, lasik_dim.y - w, lasik_dim.z]) cube([5, w, h]);
        }
    }
}

module base() {
    edge = (base_dim.x - hole_dim.x) / 2;
    translate([
        -edge,
        -base_dim.y + hole_dim.y + edge,
        -base_dim.z])
    {
        color("black") cube(base_dim);
        
        translate([0,0,base_depth])
        color("wheat") cube([base_dim.x, base_depth, det_z]);
        
        translate([0,base_dim.y - base_depth,base_depth])
        color("wheat") cube([base_dim.x, base_depth, det_z]);
        
        translate([0,base_depth,base_depth])
        color("wheat") cube([base_depth, base_dim.y - 2 * base_depth, det_z]);

        translate([base_dim.x - base_depth,base_depth,base_depth])
        color("wheat") cube([base_depth, base_dim.y - 2 * base_depth, det_z]);        
    }
}

module top() {
    translate([0, 0, det_z])
    difference() {
        edge = (base_dim.x - hole_dim.x) / 2;
        translate([
            -edge,
            -base_dim.y + hole_dim.y + edge,
            0])
        cube(base_dim);
        
        translate([0,0,-1]) cube([hole_dim.x, hole_dim.y, base_dim.z + 2]);
    }
}

module laser() {
    color("white") translate([-laser_dim.x,-laser_dim.y/2,0])
    difference() {
        cube(laser_dim);
        
        translate([-1, laser_dim.y/2, det_z/2])
        rotate([0,90,0])
        cylinder(r = rLaser, h = laser_dim.x + 2);
    }
    
    color("red") translate([0,0,det_z / 2]) rotate([0,90,0])
    cylinder(r = 0.5, h = hole_dim.x);
}

module full() {
    base();
        
    translate([50,-80,0]) rotate([0,0,0]) lasik();
        
    %top();

    translate([hole_dim.x,0,0]) rotate([0,0,180]) la_full(hole_dim.y);
    translate([hole_dim.x,hole_dim.y,0]) rotate([0,0,-90]) la_full(hole_dim.x);    
    translate([0, hole_dim.y, 0]) da_full();
    translate([0, 0, 0]) rotate([0,0,90]) da_full();
    
}

full();