use <BPW.scad> 

$fn = 10;

border = 1;
n = 6;
height = 24.5;
total_width = 200;
spacing = (total_width - height) / (n-1);

radius = 9;

thick = 4;
depth = radius + border;
baffle = 3;
border2 = 1; // for square holes in the white bar
hole_width = spacing - height;

r_laser = 3.0625;
w_laserslot = 2;
offset_laserslot = 0.5;

module back_f() {
    difference() {
        cube([(n-1) * spacing + height, depth, height]);
        for(i = [0:n-1]) {
            dx = i * spacing;
            translate([dx, 0, 0]) {
                translate([height/2,0,height/2]) sphere(r = radius);
                translate([height, -1, border2]) cube([hole_width, depth + 2, height - 2 * border2]);
            }
        }
    }
}

module sensors(extend=5) {
    for(i = [0:n-1]) {
        dx = i * spacing;
        translate([dx, 0, 0]) {
            translate([height/2-radius, 0, height/2]) rotate([0,90,0]) BPW(extend);
        }
    }
}

module front_f() {
    difference() {
        translate([0,-thick,0]) cube([(n-1) * spacing + height, thick, height]);
        for(i = [0:n-1]) {
            dx = i * spacing;
            translate([dx, 0, 0]) {               
                translate([height / 2,1,height/2]) rotate([90,0,0])
                cylinder(h = thick + 2, r = radius);
                
                y = thick - border;
                translate([height, -y, border2]) cube([hole_width, y + 1, height - 2 * border2]);
            }
        }
    }
    for(i = [0:n-1]) {
        dx = height/2 - radius + i * spacing;
        translate([dx,-thick,0]) cube([baffle, thick, height]);
    }
}

//======================================================

module da_back() color("white") difference() {
    back_f();
    sensors();
}

module da_front() color("gray") difference() {
    front_f();
    sensors(2);
}

module da_full() translate([0,thick,0]) {
    da_front();
    da_back();
}

//======================================================

module la_full(beam_l = 0) {
    color("gray") difference() {
        cube([(n-1) * spacing + height, depth, height]);
        for(i = [-1:n-1]) {
            dx = i * spacing;
            dx0 = height/2 + r_laser + border;
            hole_width = spacing - 2 * (r_laser + border);
            translate([dx, 0, 0]) {
                translate([height/2, -1, height/2]) rotate([-90,0,0])
                cylinder(r = r_laser, h = depth + 2);
                
                translate([height/2 - w_laserslot/2 + offset_laserslot, border, border])
                cube([w_laserslot,depth-border+1,height - 2 * border]);
                
                translate([dx0, border, border2]) cube([hole_width, depth, height - 2 * border2]);
            }
        }
    }

    if (beam_l > 0) color("red") for(i = [0:n-1]) {
        dx = i * spacing;
        #translate([height/2 + dx,0,height/2]) rotate([90,0,0]) cylinder(r = 1, h = beam_l);

    }
}

//======================================================

da_full();
translate([0,-100, 0]) mirror([0,1,0]) la_full(100 + radius);